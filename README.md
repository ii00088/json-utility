# JSON Utility
## Usage
```
$ make build
$ make run mode=TEST file=example.json #TESTING WITH LOCAL FILE. Where example.json is the file we want to process
$ make run mode=CONTAINER #RUN AS REST API. 
```

## Testing
```
$ make test
```
## Containerisation
```
docker-compose up -d
```
## Testing the REST API
Testing the Rest API happens by using the endpoint which should be localhost:8080
```
curl -X POST -H 'Content-Type: application/json' -d @example.json localhost:8080
```
## Discussion
Designing the solution is made based on the assumption that the load will be a JSON file containing an array of all the entries needed to perform the calculations. The code can run in 2 modes:
- TEST: this mode allows you to test the utility using a sample JSON file from the local file system
- CONTAINER: this mode will make the app run as a rest API. Its also used when building a Docker container.

Scaling: 
Scalability can be achieved by deploying the app to a Kubernetes cluster so it's available and easily orchestrated. 
Then it's probably a good idea to replicate the container to ensure redundancy. We can also think about who is using the service and distribute the replicas across multiple geographical locations where needed. The data ideally would be cached somewhere close to the container so that the latency is low. 

Maybe a good solution for Qubit would be a different environment such as BigTable and BigQuery where we can query all the unique entries of websites, compound that with entries per day and then by unique users.

Maybe a good solution for another environemnt can be to be stored
## Dependencies
- You'd need to have GOPATH exported.  GOBIN for MACOS for go get command