package main

import (
	"log"
	"sync"
	"time"
)

//Process handles processing a load given by
func ProcessLoad(visited *Links, load Load, wg *sync.WaitGroup) {
	//Notify main that this routine is done.
	defer wg.Done()
	//Time should be accepted both in epoch milliseconds or seconds.
	var t string
	if time.Now().UnixNano() > load.Timestamp {
		t = time.Unix(load.Timestamp/1000, 0).Format("2006-01-02")
	} else {
		t = time.Unix(load.Timestamp, 0).Format("2006-01-02")
	}

	visited.mux.Lock()

	if date, ok := visited.URL[load.URL]; ok {
		log.Println("URL exists.")
		if visit, ok := date.Date[t]; ok {
			log.Println("Date exists.")
			if visit.UserIds[load.Userid] {
				visited.mux.Unlock()
				return //Return as the user has already visited that website for this day once.
			} else {

				visit.mux.Lock()
				visit.Counter += 1
				visit.UserIds[load.Userid] = true

				visited.mux.Unlock()
				visit.mux.Unlock()
				return
			}
		} else {
			//Add the Date and a Visit.
			entry := Visits{Counter: 0, UserIds: make(map[string]bool)}
			entry.mux.Lock()
			entry.Counter += 1
			entry.UserIds[load.Userid] = true
			entry.mux.Unlock()

			visited.URL[load.URL].Date[t] = &entry
			visited.mux.Unlock()

			log.Println("Date and Visit are added.")
			return
		}
	} else {
		log.Println("URL doesn't exist.")
		//Initialising URL, date and visits for that date.

		entry := Visits{Counter: 0, UserIds: make(map[string]bool)}
		entry.mux.Lock()

		dates := Dates{Date: make(map[string]*Visits)}

		entry.Counter += 1
		entry.UserIds[load.Userid] = true
		entry.mux.Unlock()

		dates.Date[t] = &entry

		visited.URL[load.URL] = &dates
		visited.mux.Unlock()
		return
	}
}
