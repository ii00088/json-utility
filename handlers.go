package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"sync"
)

//Handles opens the JSON file and calls extractJSON to parse
//the data passed as file. Returns the result as a struct.
func Handles(argument string) (urls *Links, err error) {
	jsonFile, err := os.Open(argument)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	log.Println("Successfully Opened", argument)
	//Close when done.
	defer jsonFile.Close()
	payload := ExctractJSON(jsonFile)
	result := ProcessJson(payload)
	return result, err
}

//Web handler for requests containing JSON input.
func Index(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var loads []Load

	err := decoder.Decode(&loads)
	if err != nil {
		panic(err)
	}

	result := ProcessJson(loads)
	WriteResult(w, result)
}

//Preocessing the JSON as a whole array.
func ProcessJson(loads []Load) (links *Links) {
	visited := Links{URL: make(map[string]*Dates)}

	wg := new(sync.WaitGroup)

	for _, load := range loads {
		if load.TypeRequest != "GET" {
			log.Println("Request is invalid: ", load.TypeRequest)
			continue
		}
		wg.Add(1)
		go ProcessLoad(&visited, load, wg)

	}
	wg.Wait()
	return &visited
}
