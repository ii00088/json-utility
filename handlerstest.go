package main

import (
	"fmt"
	"log"
	"testing"
)

/*
Standard assert Equals function
*/
func assertEqual(t *testing.T, a interface{}, b interface{}, message string) {
	if a == b {
		return
	}
	if len(message) == 0 {
		message = fmt.Sprintf("%v != %v", a, b)
	}
	t.Fatal(message)
}

/*
Testing with sample JSON tha websites are visited.
*/
func TestWebsites(t *testing.T) {
	result, err := Handles("test/test-websites.json")
	if err != nil {
		log.Println(err)
		return
	}

	assertEqual(t, len(result.URL), 1, "")
}

/*
Testing if a user visits one URL multiple times but in
different days if it's still going to add it.
*/
func TestWebsites2(t *testing.T) {
	result, err := Handles("test/test-websites2.json")
	if err != nil {
		log.Println(err)
		return
	}
	assertEqual(t, len(result.URL), 1, "")
}

/*
Testing if each date gets added. Basic case. 1 user visits 1 page on
2 different dates. 2 Dates should be returned. Also checks if POST
requests get dropped.
*/
func TestDates(t *testing.T) {
	result, err := Handles("test/test-dates.json")
	if err != nil {
		log.Println(err)
		return
	}
	assertEqual(t, len(result.URL["http://www.someamazingwebsite.com/1"].Date), 2, "")
}

/*
Testing if each date gets added. Advaced case. 3 user visits 2 page on
3 different dates. 2 Dates should be returned for each website.
*/
func TestDates2(t *testing.T) {
	result, err := Handles("test/test-dates2.json")
	if err != nil {
		log.Println(err)
		return
	}
	assertEqual(t, len(result.URL["http://www.someamazingwebsite.com/1"].Date), 2, "")
	assertEqual(t, len(result.URL["http://www.someamazingwebsite.com/2"].Date), 2, "")
}
