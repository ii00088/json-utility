package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

//Prints the results for a given structure containing already processed Links.
func PrintResult(result *Links) {
	for k, v := range result.URL {
		log.Println("For URL: ", k)
		for m, x := range v.Date {
			log.Println("↳", m, "there are:", x.Counter, "unique visits.")
		}
	}
}

//Function for returning response to the user if the code runs in container
//as an API
func WriteResult(w http.ResponseWriter, result *Links) {
	for k, v := range result.URL {
		fmt.Fprintf(w, "For URL, %v\n", k)
		log.Println("For URL: ", k)
		for m, x := range v.Date {
			fmt.Fprintf(w, "↳%v there are: %v unique visits.\n", m, x.Counter)
			log.Println("↳", m, "there are:", x.Counter, "unique visits.")
		}
	}
}

//Extracts JSON payload and calls Process to handle the data.
func ExctractJSON(file *os.File) (load []Load) {
	d := json.NewDecoder(file)

	var loads []Load

	err := d.Decode(&loads)
	if err != nil {
		log.Panic(err)
	}
	return loads
}
