// Package implments JSON parser utility to process a stream of JSON messages
// and calculate the number of unique viewers per day for each URL in the stream.
package main

import (
	"log"
	"net/http"
	"os"
	"sync"

	"github.com/gorilla/mux"
)

type Load struct {
	Userid      string `json:"userid"`
	URL         string `json:"url"`
	TypeRequest string `json:"type"`
	Timestamp   int64  `json:"timestamp"`
}

type Links struct {
	URL map[string]*Dates //map holding all the Dates a certain URL is visited
	mux sync.RWMutex      //mutex for race condition
}
type Dates struct {
	Date map[string]*Visits //map holding all visits for certain date
}

type Visits struct {
	mux     sync.RWMutex    //mutex for race condition
	Counter int             //Counter for unique visits
	UserIds map[string]bool //map holding different user ids
}

func main() {
	if os.Args[1] == "CONTAINER" {
		router := mux.NewRouter().StrictSlash(true)
		router.HandleFunc("/", Index)
		log.Fatal(http.ListenAndServe(":8080", router))
	} else {
		//Check if the usage of the script has been correct.
		if len(os.Args) != 3 {
			log.Println("USAGE: ./json-utility [mode] [path/to/file]")
			return
		}

		//Get the result from a file.
		result, err := Handles(os.Args[2])
		if err != nil {
			log.Println(err)
			return
		}

		//Print the result
		PrintResult(result)
	}

}
